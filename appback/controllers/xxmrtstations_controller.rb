class MrtstationsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @mrtstation_pages, @mrtstations = paginate :mrtstations, :per_page => 10
  end

  def show
    @mrtstation = Mrtstation.find(params[:id])
  end

  def new
    @mrtstation = Mrtstation.new
  end

  def create
    @mrtstation = Mrtstation.new(params[:mrtstation])
    if @mrtstation.save
      flash[:notice] = 'Mrtstation was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @mrtstation = Mrtstation.find(params[:id])
  end

  def update
    @mrtstation = Mrtstation.find(params[:id])
    if @mrtstation.update_attributes(params[:mrtstation])
      flash[:notice] = 'Mrtstation was successfully updated.'
      redirect_to :action => 'show', :id => @mrtstation
    else
      render :action => 'edit'
    end
  end

  def destroy
    Mrtstation.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
