class Mycomment < ActiveRecord::Base
validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
validates_presence_of :desc,  :email, :title


belongs_to :member

def self.update_by_sql(sql)
         connection.update(sql)
end

end
