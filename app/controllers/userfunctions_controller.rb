require 'muni_mods'
class UserfunctionsController < ApplicationController
before_filter :check_authentication, :except => [:signin_form, :signin]

def check_authentication
	unless session[:member]
	session[:intended_action] = action_name
	
	redirect_to :action => "signin_form"
	end
end

def signin_form
end


def delmyprofile

@delpro = Deleteprofile.new	
render(:layout => false)

end

def confdelprofile

 @myad = Ad.find(:first, :conditions => '  member_id =' + session[:member].to_s)
 
 if @myad
 	
 	@mydel = Deleteprofile.new(params[:delpro])
 	
    
 	
 		@mydel.member_id = session[:member]
 	if @mydel.save
 	@myad.destroy
 	flash[:notice] = 'Profile Deleted!'
 		else
 			
 		flash[:notice] = 'Please type the reason for deleting'
 	
 	end
 	
 end
 	
redirect_to '/controlpanel'
end


def putwatermark(myf, myid)
	mys1 = myid.to_s
	
	myf=myf.gsub(mys1,mys1+'/medium')
	
	
	require 'RMagick'  
	
	
	dst = Magick::Image.read(myf).first
        src = 
Magick::Image.read('/home/brahminsmatrimony.com/test/public/images/icons/water.gif').first
        result = dst.composite(src, Magick::SouthGravity ,
Magick::OverCompositeOp)
        result.write(myf)
    end
    

def doallwater
@myas = Ad.find(:all, :conditions => 'id < 29349 and image1 is not null')
for myad in @myas
addwatermark(myad.id)
end
redirect_to '/'
end


def addwatermark(myid)
	
	@myad = Ad.find(myid)
	
	if @myad
		ad = @myad
		if @myad.image1 
			myff = @myad.image1
			putwatermark(myff, @myad.id)
		end
		
		if @myad.image2 
			myff = @myad.image2
			putwatermark(myff, @myad.id)
		end
		
		if @myad.image3
			myff = @myad.image3
			putwatermark(myff, @myad.id)
		end
		
		if @myad.image4
			myff = @myad.image4
			putwatermark(myff, @myad.id)
		end
		
		if @myad.image5
			myff = @myad.image5
			putwatermark(myff, @myad.id)
		end
		
		if @myad.image6
			myff = @myad.image6
			putwatermark(myff, @myad.id)
		end
		
	end
	

end




def myints
@myuser = Member.find(session[:member])
@isends = Expint.find(:all, :conditions => 'fromid = ' + @myuser.id.to_s, :order => 'toid desc')
@usends = Expint.find(:all, :conditions => 'toid = ' + @myuser.id.to_s, :order => 'fromid desc')
end

def getconfirm

@myuser = Member.find(session[:member])
flash[:notice] = "Please follow the instructions sent to " + @myuser.email + " to confirm your email"
		
      	@mysub = "Click on http://www.brahminsmatrimony.com/confirm/" + @myuser.id.to_s + "/" +
		         @myuser.rand1.to_s + "/" + @myuser.rand2.to_s
		@mybody = "Please click on the following link or copy and paste <br/> http://www.brahminsmatrimony.com/confirm/" + @myuser.id.to_s + "/" +
		         @myuser.rand1.to_s + "/" + @myuser.rand2.to_s + "<br/> Thanks <br/>Support, BrahminsMatrimony.com"
		Notifier::deliver_sendemail(@myuser.email,@mysub,@mybody)



end



def checkprof(profileid, usertype)
	@ok = 0
	
	if usertype != 1 
		@ok = 1
	else
		@ok = Ad.count(:conditions => "member_id = " +  profileid.to_s + " and status = 1" )
	end
	@ok
	
end


def expinterest
	@toprofile = params[:id]
	@myurl = request.env["HTTP_REFERER"] 
	@myuser = Member.find(session[:member])
	@ok1 = checkprof(@myuser.id, @myuser.usertype_id)

if @toprofile == @myuser.id 
  @ok1 = 2
end


@touser = Member.find(@toprofile)

if @touser
 if @touser.sex_id == @myuser.sex_id
     @ok1 = 3
 	
 end
 	
end
		
@mycount = Expint.count(:conditions => "fromid = " + @myuser.id.to_s + " and toid = " + @touser.id.to_s)
if @mycount > 0
@ok1 = 4
end





if @ok1 == 1
	
@myint = Expint.new
@myint.fromid = @myuser.id
@myint.toid = @touser.id
@myint.save
			@mysub = "Profile ID " + @myuser.id.to_s  + " has expressed interest in your profile!"
      	@mytext = Emailtext.find(:first, :conditions => "emailid='EXPINT'")
      	@mybody = "Click or copy and paste this link http://www.brahminsmatrimony.com/myprofile/" + @myuser.id.to_s + "<br/"
		@mybody = @mybody + @mytext.contents
		Notifier::deliver_sendemail(@touser.email,@mysub,@mybody)
		
   @mymsg = "Your interest for profile " + @toprofile.to_s + " has been notified!"
elsif @ok1 == 2
    @mymsg = "You can not express interest to yourself!"
elsif @ok1 == 3
    @mymsg = "Same Gender - Can not do!"
elsif @ok1 == 4
    @mymsg = "Why Try again? You already expressed your interest to this profile!"
else
	
	@mymsg = "You must have an approved profile to express interest"
	
end	

flash[:notice] = @mymsg
redirect_to @myurl
end



def clearphoto

@myad = Ad.find(:first, :conditions => "member_id = " + session[:member].to_s + " and id = " + params[:myadid].to_s)

if @myad
	if params[:myimageid] == '1'
		Ad.update_by_sql("update ads set image1=null where id= #{params[:myadid]}") 
	end

	if params[:myimageid] == '2'
		Ad.update_by_sql("update ads set image2=null where id= #{params[:myadid]}") 
	end
	
		if params[:myimageid] == '3'
		Ad.update_by_sql("update ads set image3=null where id= #{params[:myadid]}") 
	end
	
		if params[:myimageid] == '4'
		Ad.update_by_sql("update ads set image4=null where id= #{params[:myadid]}") 
	end
	
	if params[:myimageid] == '5'
		Ad.update_by_sql("update ads set image5=null where id= #{params[:myadid]}") 
	end	
	if params[:myimageid] == '6'
		Ad.update_by_sql("update ads set image6=null where id= #{params[:myadid]}") 
	end	
	
	if params[:myimageid] == '11'
		Ad.update_by_sql("update ads set pimage1=null where id= #{params[:myadid]}") 
	end

	if params[:myimageid] == '12'
		Ad.update_by_sql("update ads set pimage2=null where id= #{params[:myadid]}") 
	end
	
		if params[:myimageid] == '13'
		Ad.update_by_sql("update ads set pimage3=null where id= #{params[:myadid]}") 
	end


		if params[:myimageid] == '14'
		Ad.update_by_sql("update ads set himage1=null where id= #{params[:myadid]}") 
	end
	
	
		if params[:myimageid] == '15'
		Ad.update_by_sql("update ads set himage2=null where id= #{params[:myadid]}") 
	end
	
		
	flash[:notice] = "Image " + params[:myimageid] + " deleted "
end


@myurl = request.env["HTTP_REFERER"] 
redirect_to @myurl
end



def privatephoto

if session[:superuser]
	@myad = Ad.find(:first, :conditions => " id = " + params[:myprofileid].to_s)

else
	
@myad = Ad.find(:first, :conditions => " id = " + params[:myprofileid].to_s + " and privatepass = '" + params[:privpassword] + "'")
end

if @myad
else
flash[:notice] = "Invalid Password"
@myurl = request.env["HTTP_REFERER"] 
redirect_to @myurl
end


end



def firstscreen

redirect_to '/controlpanel'
end



def whoisfavorite

@myad = Ad.find(:first, :conditions => 'member_id = ' + session[:member].to_s)
@myvas = nil

if @myad 
@myfavs = Favorite.find(:all, :conditions => "url like '%/" + @myad.id.to_s + "/%'")
end


	
end




def index
#	redirect_to :controller => "rents101", :action => "adlist"
	redirect_to "/browse/all/all/all"
end

def signin


  	@mycount = Member.count(:conditions => "( username='"+ params[:userid]+ "'" + " or id = " + params[:userid].to_i.to_s + ") and password='"+params[:passwd]+"'")

	if @mycount == 0
		flash[:notice] = "Invalid Username/Password"
		redirect_to :action => 'signin_form'
	else
		

		session[:member] = Member.authenticate(params[:userid], params[:passwd]).id
		if !session[:member]
			flash[:notice] = "Invalid Username/Password"
			redirect_to :action => 'signin_form'
		end

#
#  get the superuser field and set it
#
 		@myuser = Member.find(session[:member])

# 
#  check job seeker or employer
#

	
#
# log the login entry
#
	
	@logentry = Lastlogin.new
	@logentry.member_id = @myuser.id
	@logentry.ip_address = request.remote_ip
	@logentry.save
	

		if @myuser.superuser == 101
			session[:superuser] = 101
		else
			session[:superuser] = nil
		end


		@mynewcount = Pmessage.find_unreadcount(session[:member])

@mydestination = '/firstscreen'
		
#
#  Check for paid members
#		

if @myuser.usertype_id == 1

@diff = Time.now.to_date  - @myuser.created_on.to_date
if @diff > 14
	session[:savedmember] = session[:member]
	session[:member] = nil
	session[:superuser] = nil
	flash[:notice] = 'Profile ID : ' + @myuser.id.to_s + ' - Your Free Trial Period Expired! Pay Now to access your account'
	
@mydestination = '/paynow'
else
	if @myuser.confirmed == 0
		session[:member] = nil
		session[:confirmuser] = @myuser.id
		@mydestination = '/confirmcode'
	else
		
#	   flash[:notice] = 'Your Free Trial Period is going to expire in ' + (14-@diff).to_s + ' Days - Renew now!'
	   @mydestination = '/controlpanel'
    end

end



end

        
					redirect_to @mydestination


	end

end




 def editprofile
#    @member = Member.find(params[:id])
    @member = Member.find(session[:member])

  end

  def updateprofile
    @member = Member.find(params[:id])
    if @member.update_attributes(params[:member])
      flash[:notice] = 'Member was successfully updated.'
      render :action => 'editprofile'
    else
      render :action => 'editprofile'
    end
  end


  def adnew
    @mycount = Ad.count(:conditions => "member_id= " + session[:member].to_s)
    @member = Member.find(session[:member], :include => [:membertype])
    if @member.membertype.adsallowed <= @mycount 
		flash[:notice] = "You exceeded the Ad Limit. Please contact Sales at support@brahminsmatrimony.com to post more ads for a fee"
	      redirect_to :action => 'myads'

    else	
		@mylat = @member.state.lat if @member.state.lat 
 		@mylng = @member.state.lat  if @member.state.lng

    		@ad = Ad.new
    end


  end

  def adcreate
    @ad = Ad.new(params[:ad]) 
    @ad.lat = session[:lat] if session[:lat] 
    @ad.lng  = session[:lng]  if session[:lng]
    @ad.maptitle = session[:maptitle]   if session[:maptitle]
    @ad.maplandmark = session[:maplandmark]   if session[:maplandmark]

    @ad.member_id = session[:member]
    if @ad.save
      flash[:notice] = 'Ad was successfully created. It will be manually processed and approved within 24 hours'
      redirect_to :action => 'myads'
	@mysub = 'New Ad Posted ' + @ad.title
	@mybody = @ad.title

    else
      render :action => 'adnew'
    end
  end

def logout
      if session[:employer] 
		session[:employer]  = nil
	end
	session[:seeker] = nil
	session[:member] = nil
	session[:superuser] =nil
	redirect_to "/login"

end


  def adedit
    @ad = Ad.find(params[:id])
 
	@mylat = @ad.adcity.lat if @ad.adcity.lat 
 	@mylng = @ad.adcity.lng if @ad.adcity.lng
	@mylat = @ad.lat if @ad.lat
	@mylng = @ad.lng if @ad.lng

   
    

    if @ad.member_id != session[:member]
		if session[:superuser] then
		else
			 flash[:notice] = "Access Disallowed"
			 redirect_to :action => 'myads'
		end
     end

  end

  def adupdate
    @ad = Ad.find(params[:id])
    @ad.lat = session[:lat] if session[:lat] 
    @ad.lng  = session[:lng]  if session[:lng]
    @ad.maptitle = session[:maptitle]   if session[:maptitle]
    @ad.maplandmark = session[:maplandmark]   if session[:maplandmark]
    @ad.status = 0
    if session[:superuser]
    	@ad.status = 1
	end
	
	expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'ad'+@ad.id.to_s)
	expire_fragment(:controller => 'rents101', :action => 'faq', :part => 'singleadad'+@ad.id.to_s)
	
    if @ad.update_attributes(params[:ad])
    	if @ad.member.usertype_id == 2
    		flash[:notice] = 'Ad was successfully updated '
		else
			flash[:notice] = 'Ad was successfully updated - It will be activated within 24 hours'
		end
		
      
      redirect_to :controller => 'userfunctions', :action => 'myads', :id => @ad
    else
      render :action => 'edit'
    end
  end

  def myads
	@myid = session[:member]
  	@mycount = Ad.count(:conditions => "member_id=" + @myid.to_s)

#	flash[:notice] = 'count ' + @mycount.to_s

	 @ad_pages, @ads = paginate(:ads, :per_page => 9, :order => "id DESC", :conditions => ["member_id=?", @myid])

  end

  def addestroy
        @ad = Ad.find(params[:id])
   
    if session[:superuser]
		    Ad.find(params[:id]).destroy
	    	redirect_to "/browse/all/all/all"
	else
	    if @ad.member_id != session[:member] 
			 flash[:notice] = "Access Disallowed"
	    else
		    Ad.find(params[:id]).destroy

	     end
	    redirect_to :action => 'myads'
	end

  end


  def adapprove
	if session[:superuser] 
		@tmpad = Ad.find(params[:id])
		@tmpad.status = 1
		@tmpad.update_attributes(:status => @tmpad.status)
		addwatermark(@tmpad.id)
		flash[:notice] = "Ad Approved"	
     
		@mysub = "Your Profile in brahminsmatrimony.com has been Approved"
      	@m1 = Emailtext.find(:first, :conditions => "emailid='NEWAD'")
      	@mybody = @m1.contents

		
		if  @tmpad.verifiedprofile != 1
			@mysub = 'Action Required - Verify your Mobile Number - BrahminsMatrimony.com'
			@mybody = 'Verify Your Mobile - Send an SMS with your Profile ID ' + @tmpad.member_id.to_s + ' to 09176623938' + @mybody
	   	end
	   			
		Notifier::deliver_sendemail(@tmpad.member.email,@mysub,@mybody)


	else
		flash[:notice] = "access denied"
	end
	    	redirect_to "/browse/all/all/all"


   end

def pmnew
    @pmessage = Pmessage.new

	
	    @mycount = Ad.count(:conditions => "id = " + params[:ad_id].to_s)
		if @mycount > 0 then
	    		@tmpad  = Ad.find(params[:ad_id])
	
		end

@myuser = Member.find(session[:member])

   if @myuser.usertype_id == 1
   	
   	  flash[:notice] = "Only Paid Regular Members are allowed to send PM - Upgrade your Account "
   	  redirect_to "/paynow"
   	end



	


	if session[:member] == @tmpad.member_id 
		flash[:notice] = "Can not send PM to yourself "
		redirect_to :controller => 'rents101', :action => 'adlist'

	end

      session[:topmid] = @tmpad.member_id 

end

  def pmcreate
    @pmessage = Pmessage.new(params[:pmessage])
    @pmessage.from_user = session[:member]
    @pmessage.to_user = session[:topmid]
    
#    @pmessage.ad_id = @tmpad.id

    if @pmessage.save
      flash[:notice] = 'Private Message Sent.'
	@fromuser = Member.find(:first, :conditions => ["id=?", @pmessage.from_user])
	@touser = Member.find(:first, :conditions => ["id=?", @pmessage.to_user])

      @mytext = Emailtext.find(:first, :conditions => "emailid='NEWPM'")
      @mybody = @mytext.contents

	@mysub = 'PM From ' + @fromuser.username + ' To ' + @touser.username
		@mysub = 'You have a new Private Message from Profile ID : '+ @fromuser.id.to_s
		Notifier::deliver_sendemail(@touser.email,@mysub,@mybody)
Notifier::deliver_sendemail('harinilimited@yahoo.com',@mysub,@pmessage.description)

      redirect_to '/mypms'
    else
      flash[:notice] = 'Pmessage Had errors.'
      redirect_to :controller => 'rents101', :action => 'adlist'
    end
  end

 def pmreply
    @pmessage = Pmessage.new


  end

  def pmreplycreate
    @pmessage = Pmessage.new(params[:pmessage])
    @pmessage.from_user = session[:member]
    @pmessage.to_user = params[:reply_member_id]

    if @pmessage.save
	@fromuser = Member.find(:first, :conditions => ["id=?", @pmessage.from_user])
	@touser = Member.find(:first, :conditions => ["id=?", @pmessage.to_user])

      flash[:notice] = 'Pmessage was successfully created.'
	@mysub = 'PM Reply From  Profile ID ' + @fromuser.id.to_s + ' To Profile ID ' + @touser.id.to_s
	@mybody = @pmessage.description
		Notifier::deliver_sendemail('bwconsultant2000@yahoo.com',@mysub,@mybody)
	@mybody = 'Please check your Private Messages <a href="http://www.brahminsmatrimony.com/mypms">here</a>'
	@mysub = 'You got a new PM Reply from '+ @fromuser.username
		Notifier::deliver_sendemail(@touser.email,@mysub,@mybody)

      redirect_to :controller => 'userfunctions', :action => 'mypms'
    else
      flash[:notice] = 'Pmessage Had errors.'
      redirect_to :controller => 'userfunctions', :action => 'mypms'
    end
  end


	def mypms
		@myinbox = Pmessage.find_inbox(session[:member])
		@mynewcount = Pmessage.find_unreadcount(session[:member])
		@mysentbox = Pmessage.find_sentbox(session[:member])
		

	end


	def mypmsnew
		@myinbox = Pmessage.find_inbox(session[:member])
		@mynewcount = Pmessage.find_unreadcount(session[:member])
		@mysentbox = Pmessage.find_sentbox(session[:member])
		

	end



	def pmdelete 
           @mypm = Pmessage.find(params[:id])
	if params[:boxid] == 'inbox'
		if @mypm.to_user != session[:member]
			flash[:notice] = 'Access Not allowed'
		else
		    @mypm.update_attributes(:receiver_delete => 1)  
		end
	else
		if @mypm.from_user != session[:member]
			flash[:notice] = 'Access Not allowed'
		else
		    @mypm.update_attributes(:sender_delete => 1)  
		end
	end
			
       redirect_to :controller => 'userfunctions', :action => 'mypms'


	end


	def addfavorite
		@myurl = request.env["HTTP_REFERER"] 
	  	@mycount = Favorite.count(:conditions => "member_id="+session[:member].to_s + " and url='" + @myurl + "'")
		if @mycount > 0
			flash[:notice] = "Already in your favorite"
			redirect_to @myurl

		else
			@favorite = Favorite.new
			@favorite.member_id = session[:member]
			@favorite.url = @myurl
			if @favorite.save!
				flash[:notice] = "Added to your Favorites"
			end

			redirect_to :action => 'myfavorites'
		end

	end
		

	def myfavorites
		@myfavorites = Favorite.find(:all, :conditions => ["member_id=?", session[:member]], :order => "created_on DESC")
		

	end


	def delfavorite
		@myfav = Favorite.find(params[:id])
		if @myfav.member_id != session[:member]
	 		flash[:notice] = "Access Disallowed"
    		else
	    		Favorite.find(params[:id]).destroy

     		end
    		redirect_to :action => 'myfavorites'
	end




	def mysearches	
		@myconditions = ' member_id =  ' + session[:member].to_s + ' and savedname > "" '
		@savedsearches = Savedsearch.find(:all, :conditions => @myconditions, :order => 'id DESC')
	end



  def savedsearchdestroy
    @savedsearch = Savedsearch.find(params[:id])
    if @savedsearch.member_id != session[:member]
	 flash[:notice] = "Access Disallowed"
	 redirect_to :action => 'mysearches'
     end

    Savedsearch.find(params[:id]).destroy
    redirect_to :action => 'mysearches'
  end


  def savedsearchedit
    @savedsearch = Savedsearch.find(params[:id])
    if @savedsearch.member_id != session[:member]
	 flash[:notice] = "Access Disallowed"
	 redirect_to :action => 'mysearches'
     end

  end


  def savedsearchsearch
    @savedsearch = Savedsearch.find(params[:id])

    if @savedsearch.member_id != session[:member]
	 flash[:notice] = "Access Disallowed"
	 redirect_to :action => 'mysearches'
     end

	session[:mysearch] = @savedsearch.searchterms
	mysavedsearch = @savedsearch
	  session[:mywhere] = Munimod.muniwhere(mysavedsearch, 1)


      redirect_to :controller => 'rents101', :action => 'searchads'



	

  end



  def savedsearchupdate
    @savedsearch = Savedsearch.find(params[:id])
    if @savedsearch.update_attributes(params[:savedsearch])
      flash[:notice] = 'Savedsearch was successfully updated.'
     end
 	 redirect_to :action => 'mysearches'


 end


def hideme
	render(:layout => false)

end

def pm_details

	@tmppm = Pmessage.find(params[:id])
	if !@tmppm.read_on 
		@tmppm.read_on = Time.now
		@tmppm.update_attributes(:read_on => @tmppm.read_on) 
	end

	render(:layout => false)


end


def   get_myinbox_unreadcount

@myinbcount = Pmessage.count(:conditions => "to_user="+session[:member].to_s  + " and read_on is null and receiver_delete = 0 ")


end


def mapcreate
marker = params[:m]
res={:success=>true,:content=>"Hello"+  marker['found']}

session[:lat]  = marker['lat']
session[:lng] = marker['lng']
session[:maptitle]  = marker['found']
session[:maplandmark]  = marker['left']
render :text=>res.to_json
end


def mapme
    @mycount = Ad.count(:conditions => "member_id="+ session[:member].to_s)
    @member = Member.find(session[:member], :include => [:membertype])
    	if @member.membertype.adsallowed <= @mycount 
		flash[:notice] = "You exceeded the Ad Limit. Please contact Sales at support@brahminsmatrimony.com to post more ads for a fee"
	      redirect_to :action => 'myads'

    	else	

	    @ad = Ad.new
	end
     
end

def controlpanel
	@myuser = Member.find(session[:member])
	@mycount = Ad.count(:conditions => 'member_id = ' + @myuser.id.to_s)
	if @mycount ==  0 and !session[:superuser]
	flash[:notice] = 'You must complete your profile first'
	redirect_to '/editmyprofile'
    end
	
end



end


