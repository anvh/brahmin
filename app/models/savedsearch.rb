class Savedsearch < ActiveRecord::Base
belongs_to :member
belongs_to :jbstate
belongs_to :agerange1
belongs_to :agerange2

has_and_belongs_to_many :maritalstatuses, :join_table => "savedsearches_maritalstatuses"
has_and_belongs_to_many :birthstars, :join_table => "savedsearches_birthstars"


def self.update_by_sql(sql)
         connection.update(sql)
end
end
