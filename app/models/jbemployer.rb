class Jbemployer < ActiveRecord::Base
require 'RMagick'
include Magick



belongs_to :member
belongs_to :adcity
belongs_to :jbcountry
belongs_to :jbstate

validates_presence_of :company_name, :main_phone, :address1, :zip, :profile


        file_column :logo, :magick => { 
          :versions => { "thumb" => "90x90", "medium" => "215x88>" }
        }


        file_column :small_logo, :magick => { 
          :versions => { "thumb" => "90x90", "medium" => "100x35>" }
        }




end

