<?php 
/**
 * [PHPFOX_HEADER]
 * 
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond_Benc
 * @package 		Phpfox
 * @version 		$Id: register-top.html.php 3382 2011-10-31 11:53:10Z Raymond_Benc $
 */
 
defined('PHPFOX') or exit('NO DICE!'); 

?>
		<div id="header_user_register">
			<div class="holder">
				<div id="header_user_register_holder">
					<a href="{url link='user.register'}">{phrase var='user.sign_up'}</a> {phrase var='user.ssitetitle_helps_you_connect_and_share_with_the_people_in_your_life' sSiteTitle=$sSiteTitle}
				</div>
			</div>
		</div>