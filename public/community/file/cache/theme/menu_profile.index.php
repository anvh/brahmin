<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php $aContent = array (
  12 => 
  array (
    'menu_id' => '12',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_profile',
    'disallow_access' => NULL,
    'module' => 'profile',
    'url' => 'profile',
    'module_is_active' => '1',
  ),
  34 => 
  array (
    'menu_id' => '34',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_blogs',
    'disallow_access' => NULL,
    'module' => 'blog',
    'url' => 'profile.blog',
    'module_is_active' => '1',
  ),
  41 => 
  array (
    'menu_id' => '41',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_friend_friends',
    'disallow_access' => NULL,
    'module' => 'friend',
    'url' => 'profile.friend',
    'module_is_active' => '1',
  ),
  55 => 
  array (
    'menu_id' => '55',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_photos',
    'disallow_access' => NULL,
    'module' => 'photo',
    'url' => 'profile.photo',
    'module_is_active' => '1',
  ),
  59 => 
  array (
    'menu_id' => '59',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_polls',
    'disallow_access' => NULL,
    'module' => 'poll',
    'url' => 'profile.poll',
    'module_is_active' => '1',
  ),
  61 => 
  array (
    'menu_id' => '61',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_profile_quiz',
    'disallow_access' => NULL,
    'module' => 'quiz',
    'url' => 'profile.quiz',
    'module_is_active' => '1',
  ),
  67 => 
  array (
    'menu_id' => '67',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_videos',
    'disallow_access' => NULL,
    'module' => 'video',
    'url' => 'profile.video',
    'module_is_active' => '1',
  ),
  38 => 
  array (
    'menu_id' => '38',
    'parent_id' => '0',
    'm_connection' => 'profile',
    'var_name' => 'menu_favorites',
    'disallow_access' => NULL,
    'module' => 'favorite',
    'url' => 'profile.favorite',
    'module_is_active' => '1',
  ),
); ?>