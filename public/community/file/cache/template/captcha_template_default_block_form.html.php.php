<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php /* Cached: March 10, 2012, 9:36 pm */ ?>
<?php 
/**
 * [PHPFOX_HEADER]
 * 
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond Benc
 * @package  		Module_Captcha
 * @version 		$Id: form.html.php 3533 2011-11-21 14:07:21Z Raymond_Benc $
 */
 
 

?>
<div class="table_clear">
	<div class="captcha_holder">
		<div class="captcha_title"><?php echo Phpfox::getPhrase('captcha.captcha_challenge'); ?></div>
<?php if (Phpfox ::getParam('captcha.recaptcha')): ?>
			<div id="js_recaptcha_holder" style="direction:ltr;">
<?php echo $this->_aVars['sCaptchaData']; ?>
			</div>
<?php else: ?>
			<div class="go_left">
				<a href="#" onclick="$('#js_captcha_process').html($.ajaxProcess('<?php echo Phpfox::getPhrase('captcha.refreshing_image', array('phpfox_squote' => true)); ?>')); $('#js_captcha_image').ajaxCall('captcha.reload', 'sId=js_captcha_image&amp;sInput=image_verification'); return false;"><img src="<?php echo $this->_aVars['sImage']; ?>" alt="<?php echo Phpfox::getPhrase('captcha.reload_image'); ?>" id="js_captcha_image" class="captcha" title="<?php echo Phpfox::getPhrase('captcha.click_refresh_image'); ?>" /></a>
			</div>
			<a href="#" onclick="$('#js_captcha_process').html($.ajaxProcess('<?php echo Phpfox::getPhrase('captcha.refreshing_image', array('phpfox_squote' => true)); ?>')); $('#js_captcha_image').ajaxCall('captcha.reload', 'sId=js_captcha_image&amp;sInput=image_verification'); return false;" title="<?php echo Phpfox::getPhrase('captcha.click_refresh_image', array('phpfox_squote' => true)); ?>"><?php echo Phpfox::getLib('phpfox.image.helper')->display(array('theme' => 'misc/reload.gif','alt' => 'Reload')); ?></a>		
			<span id="js_captcha_process"></span>
			<div class="clear"></div>
			<div class="captcha_form">
				<input type="text" name="val[image_verification]" size="10" id="image_verification" />
				<div class="extra_info">
<?php echo Phpfox::getPhrase('captcha.type_verification_code_above'); ?>
				</div>
			</div>
			<script type="text/javascript">
				$('#image_verification').attr('autocomplete','off');
			</script>
<?php endif; ?>
	</div>
</div>
