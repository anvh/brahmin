<?php defined('PHPFOX') or exit('NO DICE!'); ?>
<?php /* Cached: April 2, 2012, 7:15 pm */ ?>
<?php 
/**
 * [PHPFOX_HEADER]
 * 
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond_Benc
 * @package 		Phpfox
 * @version 		$Id: pending.html.php 1578 2010-05-07 09:38:07Z Miguel_Espinoza $
 */
 
 

?>

<?php if ($this->_aVars['iStatus'] == 1): ?>
<?php echo Phpfox::getPhrase('user.this_site_is_very_concerned_about_security');  else: ?>
<?php echo Phpfox::getPhrase('user.your_account_is_pending_approval');  endif; ?>
